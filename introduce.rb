# frozen_string_literal: true

require 'colorize'
require 'launchy'

# Handles introducing oneself
module Introduce
  NAME = 'Samuel'
  class << self

    def print_fast(text_to_print)
      text_to_print.split('').each do |char|
        print char
        sleep 0.05
      end
    end

    def print_slowly(text_to_print)
      text_to_print.split('').each do |char|
        print char
        sleep 0.1
      end
    end

    # Type out line
    def type_line(line_of_text)
      print_fast line_of_text
      puts ''
    end

    # Print link straight away with newline
    def link(url)
      print url
      puts ''
    end

    def me
      print 'Hi!'.red
      sleep 1
      print_slowly " My name is #{NAME} \n"
      type_line 'I know programming languages like...'
      puts 'Ruby! E.g an RSpec test'
      puts ''
      print_fast 'expect('
      print_fast 'RestClient'.white
      print_fast '.'
      print_fast 'get'.light_red
      print_fast '(url).code).to eq '
      print_slowly "200\n".blue
      puts ''
      sleep 1
      type_line 'C#! E.g an test using fluent assertions'
      puts ''
      print_fast 'apiResult.Status.Should().Be('
      print_fast '"available"'.light_red
      print_fast ');'
      puts ''
      print 'See an example UI test with recording at '
      puts ''
      link 'https://gitlab.com/samuel-garratt/ui_recording'
      print_fast 'It saves a dashboard of results with videos which you can see at '
      link "https://samuel-garratt.gitlab.io/-/ui_recording/-/jobs/415191175/artifacts/tmp/dashboard.html \n"
      print_fast 'See my profile at '
      link 'https://www.linkedin.com/in/samuel-garratt-3437b245/'
    end
  end
end