FROM ruby:2.6

RUN mkdir /introduce
WORKDIR /introduce

COPY . /introduce

RUN gem install bundler
RUN bundle install

ENTRYPOINT ruby run.rb