require_relative '../introduce'

describe Introduce do
  it 'introduces' do
    expect do
      Introduce.me
    end.to output(/My name is Samuel/).to_stdout
  end
end
